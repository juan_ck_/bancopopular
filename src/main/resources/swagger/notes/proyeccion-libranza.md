# Implementation Notes

### Acerca de la funcionalidad expuesta
El servicio demo de libranza.
**Path parameters** No tiene parámetros.

<br/>

### URI de acceso a la API
| Método | URI |
|--------|-------------|
|GET| /party/party-data-management/v1.0 |

<br/>

### Query Parameters válidos en esta versión de la API
| Query Param |Tipo de Dato | Descripción | Valores Permitidos |
|-------------|-------------|-------------|--------------------|
|id	 | String    |Código que identifica la transaccion| |

<br/>


### Lista de Valores usadas en esta versión de la API
A continuación se listan aquellos campos que tienen más de un valor posible. Para poder acceder a la lista de valores completa por favor contactar a la mesa de gobierno.
<br/>
| tipo | 
| ------------ |
| citizenType |

<br/>

### Código de errores del Servicio en esta versión de la API
| Código | Descripción |
|--------|-------------|
| PI0001 | No se encontró resultados para los criterios de búsqueda                |
| PI0003 | Los datos proporcionados no son válidos                                  |
| PI0004 | Ocurrió un error en el servicio Externo consumido                        |
| PI0005 | Ocurrió un error en la comunicación con el BackEnd                       |
| PI0006 | Los datos proporcionados no cumplen con los criterios para ser procesado |
| PI0007 | Ocurrió un error en el Backend                                           |
| PI0009 | Problemas de Timeout en el Servicio Backend |
| PI9999 | Ocurrió un error inesperado. Por favor contactarse con Soporte Técnico   |

<br/>
