package com.bancopupular.servicios.enumeradores;

public enum SectorEnum {

	PUBLICO,
	PENSIONADO,
	EDUCATIVO
}
