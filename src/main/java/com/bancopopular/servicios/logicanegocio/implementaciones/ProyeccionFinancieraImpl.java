package com.bancopopular.servicios.logicanegocio.implementaciones;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.bancopopular.servicios.entidades.IPC;
import com.bancopopular.servicios.entidades.InfFinanciera;
import com.bancopopular.servicios.entidades.SMLV;
import com.bancopopular.servicios.entidadesdeservicio.RespuestaConsultarInformacionFinanciera;
import com.bancopopular.servicios.excepciones.BadRequestException;
import com.bancopopular.servicios.logicanegocio.ProyeccionFinanciera;
import com.bancopopular.servicios.repositorio.IIPCRepositorio;
import com.bancopopular.servicios.repositorio.IInfFinancieraRepositorio;
import com.bancopopular.servicios.repositorio.ISMLVRepositorio;
import com.bancopupular.servicios.enumeradores.SectorEnum;

@Service
public class ProyeccionFinancieraImpl implements ProyeccionFinanciera {

	@Autowired
	private IIPCRepositorio ipcRepositorio;
	@Autowired
	private ISMLVRepositorio smlvRepositorio;
	@Autowired
	private IInfFinancieraRepositorio infFinancieraRepositorio;

	private int egresos;
	private double sueldoProyectado;
	private double egresoProyectado;
	private List<String> listadoErrores;
	private int anioActual;
	private Calendar calendar;

	@Cacheable("IPC")
	@Override
	public List<IPC> consultarIPCs() {
		return ipcRepositorio.consultarIPCs();
	}

	@Cacheable("SMLV")
	@Override
	public List<SMLV> consultarSMLV() {
		return smlvRepositorio.consultarSMLV();
	}
	
	@Override
	public RespuestaConsultarInformacionFinanciera consultarInformacion(int tipoDocumento, String numeroDocumento,
			String nitPagaduria, int sector) throws BadRequestException {
		inicializarValores();

		try {
			List<InfFinanciera> listaInfFinanciera = infFinancieraRepositorio
					.consultarInformacionFinanciera(tipoDocumento, numeroDocumento, nitPagaduria, sector);

			InfFinanciera registro = new InfFinanciera();
			registro = validarFechasInformacionFinanciera(listaInfFinanciera);

			calcularIngresosEgresos(registro.getDescuentosLey(), registro.getPrestamosNomina(),
					registro.getObliHipotecas());

			manejarCalendar(registro);

			realizarProyecciones(sector, anioActual, calendar, registro, consultarIPCs(), consultarSMLV());

			return devolverRespuesta(registro);
		} catch (Exception e) {
			throw new BadRequestException("REGISTRO NO ENCONTRADO");
		}
	}

	@Override
	public RespuestaConsultarInformacionFinanciera consultarInformacionFinanciera(int tipoDocumento,
			String numeroDocumento, String nitPagaduria, int sector) throws BadRequestException {

		inicializarValores();

		try {
			List<InfFinanciera> listaInfFinanciera = infFinancieraRepositorio
					.consultarInformacionFinanciera(tipoDocumento, numeroDocumento, nitPagaduria, sector);

			InfFinanciera registro = new InfFinanciera();
			registro = validarFechasInformacionFinanciera(listaInfFinanciera);

			calcularIngresosEgresos(registro.getDescuentosLey(), registro.getPrestamosNomina(),
					registro.getObliHipotecas());

			manejarCalendar(registro);

			realizarProyecciones(sector, anioActual, calendar, registro, consultarIPCs(), consultarSMLV());

			return devolverRespuesta(registro);
		} catch (Exception e) {
			throw new BadRequestException("REGISTRO NO ENCONTRADO");
		}
	}

	@Override
	public int calcularIngresosEgresos(int descuentosLey, int prestamosNomina, int obliHipotecas) {
		egresos = descuentosLey + prestamosNomina + obliHipotecas;
		return egresos;
	}

	@Override
	public void proyectarIngresos(int sueldo, List<IPC> listaIPCs, int anio, int mes, List<SMLV> listaSMLV,
			int sector) {
		sueldoProyectado = sueldo;
		int salarioMinimo = consultarSalarioMinimoPorAnio(listaSMLV);
		if (sector < SectorEnum.EDUCATIVO.ordinal() && sector >= SectorEnum.PUBLICO.ordinal()) {
			calcularSueldoProyectado(anio, listaIPCs);
		} else if (sector == SectorEnum.EDUCATIVO.ordinal()) {
			if (anio == anioActual) {
				if (mes < 6) {
					calcularSueldoProyectado(anio - 1, listaIPCs);
				}
			} else {
				if (mes < 6) {
					calcularSueldoProyectado(anio - 1, listaIPCs);
				} else {
					calcularSueldoProyectado(anio, listaIPCs);
				}
			}
		}

		if (salarioMinimo > sueldoProyectado) {
			sueldoProyectado = salarioMinimo;
		}
	}

	@Override
	public void proyectarEgresos(double sueldoProyectado, int sueldo) {
		egresoProyectado = ((double) egresos / (double) sueldo) * sueldoProyectado;
	}

	public InfFinanciera validarFechasInformacionFinanciera(List<InfFinanciera> listaInfFinanciera) {
		if (listaInfFinanciera.size() == 1)
			return listaInfFinanciera.get(0);
		InfFinanciera registro = new InfFinanciera();
		for (int i = 1; i < listaInfFinanciera.size(); i++) {
			if (listaInfFinanciera.get(i).getFecDesprendible()
					.after(listaInfFinanciera.get(i - 1).getFecDesprendible())) {
				registro = listaInfFinanciera.get(i);
			}
		}
		return registro;
	}

	public int consultarSalarioMinimoPorAnio(List<SMLV> listaSMLV) {
		Calendar cal = Calendar.getInstance();
		int anioActual = cal.get(Calendar.YEAR);
		for (int i = 0; i < listaSMLV.size(); i++) {
			cal.setTime(listaSMLV.get(i).getFechaIni());
			if (cal.get(Calendar.YEAR) == anioActual)
				return listaSMLV.get(i).getValor();
		}
		return 0;
	}

	public void realizarProyecciones(int sector, int anioActual, Calendar calendar, InfFinanciera registro,
			List<IPC> listaIPCs, List<SMLV> listaSMLV) {
		if (sector >= SectorEnum.PUBLICO.ordinal() && sector <= SectorEnum.EDUCATIVO.ordinal()) {
			if (sector < SectorEnum.EDUCATIVO.ordinal() && sector >= SectorEnum.PUBLICO.ordinal()
					&& anioActual == calendar.get(Calendar.YEAR)) {
				manejarValoresSinProyeccion(registro);
			} else if (sector == SectorEnum.EDUCATIVO.ordinal() && anioActual == calendar.get(Calendar.YEAR)
					&& calendar.get(Calendar.MONTH) >= 6) {
				manejarValoresSinProyeccion(registro);
			} else {
				proyectarIngresos(registro.getSueldos(), listaIPCs, calendar.get(Calendar.YEAR),
						calendar.get(Calendar.MONTH), listaSMLV, sector);

				proyectarEgresos(sueldoProyectado, registro.getSueldos());
			}
		}
	}

	public void calcularSueldoProyectado(int anio, List<IPC> listaIPCs) {
		for (int i = 0; i < listaIPCs.size(); i++) {
			if (listaIPCs.get(i).getAnio() >= anio) {
				sueldoProyectado = (1 + ((double) listaIPCs.get(i).getValor() / 100)) * sueldoProyectado;
			}
		}
	}

	public RespuestaConsultarInformacionFinanciera devolverRespuesta(InfFinanciera registro) {
		RespuestaConsultarInformacionFinanciera respuesta = new RespuestaConsultarInformacionFinanciera();
		respuesta.setEgresos(egresoProyectado);
		respuesta.setSueldo(sueldoProyectado);
		respuesta.setFechaDesprendible(registro.getFecDesprendible());
		respuesta.setListadoErrores(listadoErrores);
		return respuesta;
	}

	public void inicializarValores() {
		egresos = 0;
		sueldoProyectado = 0;
		egresoProyectado = 0;
	}

	public void manejarCalendar(InfFinanciera registro) {
		calendar = Calendar.getInstance();
		anioActual = calendar.get(Calendar.YEAR);
		calendar.setTime(registro.getFecDesprendible());
	}

	public void manejarValoresSinProyeccion(InfFinanciera registro) {
		sueldoProyectado = registro.getSueldos();
		egresoProyectado = egresos;
	}
	
	public static boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
}
