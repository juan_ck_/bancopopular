package com.bancopopular.servicios.logicanegocio;

import java.util.List;

import com.bancopopular.servicios.entidades.IPC;
import com.bancopopular.servicios.entidades.SMLV;
import com.bancopopular.servicios.entidadesdeservicio.RespuestaConsultarInformacionFinanciera;
import com.bancopopular.servicios.excepciones.BadRequestException;

public interface ProyeccionFinanciera {

	public List<IPC> consultarIPCs();

	public List<SMLV> consultarSMLV();

	public RespuestaConsultarInformacionFinanciera consultarInformacionFinanciera(int tipoDocumento, String numeroDocumento,
			String nitPagaduria, int sector) throws BadRequestException;

	public int calcularIngresosEgresos(int descuentosLey, int prestamosNomina, int obliHipotecas);

	void proyectarIngresos(int sueldos, List<IPC> listaIPCs, int anio, int mes, List<SMLV> listaSMLV, int sector);

	void proyectarEgresos(double sueldoProyectado, int sueldo);
	
	public RespuestaConsultarInformacionFinanciera consultarInformacion(int tipoDocumento, String numeroDocumento,
			String nitPagaduria, int sector) throws BadRequestException;

}
