package com.bancopopular.servicios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import com.everis.deimos.runner.StarterWebApplication;

@SpringBootApplication
@EnableCaching
public class MicroserviciosApplication extends StarterWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviciosApplication.class, args);
	}

}
