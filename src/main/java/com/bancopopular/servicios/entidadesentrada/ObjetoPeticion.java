package com.bancopopular.servicios.entidadesentrada;

import javax.validation.constraints.NotNull;

public class ObjetoPeticion {

	@NotNull(message="tipo de documento no puede ser nulo")
	private int tipoDocumento;
	@NotNull(message="numero de documento no puede ser nulo")
	private String numeroDocumento;
	@NotNull(message="el nit de la pagaduria no puede ser nulo")
	private String nitPagaduria;
	@NotNull(message="el sector no puede ser nulo")
	private  int sector;
	
	public ObjetoPeticion() {
		
	}

	public int getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(int tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getNitPagaduria() {
		return nitPagaduria;
	}

	public void setNitPagaduria(String nitPagaduria) {
		this.nitPagaduria = nitPagaduria;
	}

	public int getSector() {
		return sector;
	}

	public void setSector(int sector) {
		this.sector = sector;
	}
	
	
}
