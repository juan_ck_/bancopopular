package com.bancopopular.servicios.repositorio;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bancopopular.servicios.entidades.InfFinanciera;


@Repository
public interface IInfFinancieraRepositorio extends CrudRepository<InfFinanciera, Integer> {

	@Query(value = "SELECT * FROM everis.T23INFFINANCIERA where C23TIPODOC = ?1 and C23IDTERCERO = ?2 and C23PAGADURIA = ?3", nativeQuery = true)
	public ArrayList<InfFinanciera> consultarInformacionFinanciera(int tipoDocumento, String numeroDocumento,
			String nitPagaduria, int sector);
}
