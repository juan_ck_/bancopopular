package com.bancopopular.servicios.repositorio;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bancopopular.servicios.entidades.SMLV;


@Repository
public interface ISMLVRepositorio extends CrudRepository<SMLV, Integer> {

	@Query(value="select * from everis.T24SALARIOMINIMO ORDER BY C24FECHAINI DESC", nativeQuery = true)
	public ArrayList<SMLV> consultarSMLV();
}
