package com.bancopopular.servicios.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bancopopular.servicios.entidades.IPC;


@Repository
public interface IIPCRepositorio extends CrudRepository<IPC, Integer>{

	@Query(value = "select * from everis.T24IPC ORDER BY C24ANIO ASC", nativeQuery = true)
	public List<IPC> consultarIPCs();
}
