package com.bancopopular.servicios.entidades;

import static com.everis.deimos.core.constants.HttpHeadersKey.SERVER_NAME;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiParam;


@Entity
@Table(name = "T24IPC")
public class IPC {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "C24IDIPC")
	@ApiParam(value = "Este campo es un valor estandar ya existente y sera usado como "
			+ "identificador.", name = "idIpc", type = "int", required = true, example = "1")
	private int idIpc;
	@Column(name = "C24VALOR")
	@ApiParam(value = "Este campo es el valor del IPC referenciado", name = "valor", type = "int", required = true, example = "1000")
	private double valor;
	@Column(name = "C24ANIO")
	@ApiParam(value = "Este campo es el valor anio aplicable al IPC referenciado", name = "anio", type = "int", required = true, example = "2009")
	private int anio;
	@Column(name = "C24FECHAREG")
	@ApiParam(value = "Este campo es la fecha del registro del IPC", name = "fechaReg", type = "date", required = true, example = "29/12/09")
	private Date fechaReg;
	@Column(name = "C24USUARIO")
	@ApiParam(value = "Nombre del usuario relacionado a la consulta", name = "usuario", type = "date", required = true, example = "Pepito")
	private String usuario;

	public IPC() {
		super();
	}

	public int getIdIpc() {
		return idIpc;
	}

	public void setIdIpc(int idIpc) {
		this.idIpc = idIpc;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public Date getFechaReg() {
		return fechaReg;
	}

	public void setFechaReg(Date fechaReg) {
		this.fechaReg = fechaReg;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}
