package com.bancopopular.servicios.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiParam;


@Entity
@Table(name = "T24SALARIOMINIMO")
public class SMLV {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "C24FECHAINI")
	@ApiParam(value = "Este campo es la fecha", name = "date", type = "Date", required = true, example = "29/12/09")
	private Date fechaIni;
	@Column(name = "C24VALOR")
	@ApiParam(value = "Este campo es el valor del salario minimo de respectivo anio ", name = "valor", type = "int", required = true, example = "1000")
	private int valor;

	public SMLV() {
		super();
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

}
