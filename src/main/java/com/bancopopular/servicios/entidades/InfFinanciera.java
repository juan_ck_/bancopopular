package com.bancopopular.servicios.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiParam;


@Entity
@Table(name = "T23INFFINANCIERA")
public class InfFinanciera {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "C23IDTERCERO")
	@ApiParam(value = "Este campo es el numero de documento del usuario ", name = "idIpc", type = "String", required = true, example = "1000")
	private String idTercero;
	@Column(name = "C23TIPODOC")
	@ApiParam(value = "Este campo es el tipo de documento ", name = "tipoDoc", type = "int", required = true, example = "1000")
	private int tipoDoc;
	@Column(name = "C23SUELDOS")
	@ApiParam(value = "Este campo es el valor del sueldo ", name = "sueldos", type = "int", required = true, example = "1000")
	private int sueldos;
	@Column(name = "C23BONIFICACIONES")
	@ApiParam(value = "Este campo es el valor de bonificaciones ", name = "bonificaciones", type = "int", required = true, example = "1000")
	private int bonificaciones;
	@Column(name = "C23HORASEXTRAS")
	@ApiParam(value = "Este campo es es el valor de horas extras ", name = "horasExtra", type = "int", required = true, example = "1000")
	private int horasExtra;
	@Column(name = "C23PRIMAS")
	@ApiParam(value = "Este campo es el valor de la prima", name = "primas", type = "int", required = true, example = "1000")
	private int primas;
	@Column(name = "C23HONORARIOS")
	@ApiParam(value = "Este campo es el valor de honorarios ", name = "honorarios", type = "int", required = true, example = "1000")
	private int honorarios;
	@Column(name = "C23OTROSINGRESOS")
	@ApiParam(value = "Este campo es el valor de otros ingresos ", name = "otrosIngresos", type = "int", required = true, example = "1000")
	private int otrosIngresos;
	@Column(name = "C23DESCUENTOSLEY")
	@ApiParam(value = "Este campo es el valor de descuentos ley ", name = "descuentosLey", type = "int", required = true, example = "1000")
	private int descuentosLey;
	@Column(name = "C23PRESTAMOSNOMINA")
	@ApiParam(value = "Este campo es el valor de prrestamos por nomina ", name = "prestamosNomina", type = "int", required = true, example = "1000")
	private int prestamosNomina;
	@Column(name = "C23OBLIHIPOTECAS")
	@ApiParam(value = "Este campo es el valor de obligaciones por hipoteca ", name = "obliHipotecas", type = "int", required = true, example = "1000")
	private int obliHipotecas;
	@Column(name = "C23OBLIFINANCIERAS")
	@ApiParam(value = "Este campo es el valor de obligaciones financieras ", name = "obliFinancieras", type = "int", required = true, example = "1000")
	private int obliFinancieras;
	@Column(name = "C23GASTOSFAMILIA")
	@ApiParam(value = "Este campo es el valor de gastos familiares ", name = "gatosFamilia", type = "int", required = true, example = "1000")
	private int gatosFamilia;
	@Column(name = "C23OTROSGASTOS")
	@ApiParam(value = "Este campo es el valor de otros gastos ", name = "otrosGastos", type = "int", required = true, example = "1000")
	private int otrosGastos;
	@Column(name = "C23FECDESPRENDIBLE")
	@ApiParam(value = "Este campo es la fecha del desprendible ", name = "fecDesprendible", type = "Date", required = true, example = "29/12/09")
	private Date fecDesprendible;
	@Column(name = "C23ESTADO")
	@ApiParam(value = "Este campo es el estado ", name = "estado", type = "int", required = true, example = "1000")
	private int estado;
	@Column(name = "C23FECESTADO")
	@ApiParam(value = "Este campo es la fecha de estado ", name = "fecEstado", type = "Date", required = true, example = "29/12/09")
	private Date fecEstado;
	@Column(name = "C23USUARIO")
	@ApiParam(value = "Este campo es el usuario ", name = "usuario", type = "String", required = true, example = "LTOLOSA061")
	private String usuario;
	@Column(name = "C23TERMINAL")
	@ApiParam(value = "Este campo es la terminal ", name = "terminal", type = "String", required = true, example = "LTOLOSA061")
	private String terminal;
	@Column(name = "C23PAGADURIA")
	@ApiParam(value = "Este campo es la pagaduria ", name = "pagaduria", type = "String", required = true, example = "LTOLOSA061")
	private String pagaduria;
	@Column(name = "C23CODSUCURSAL")
	@ApiParam(value = "Este campo es el codigo de la sucursal", name = "codSucursal", type = "int", required = true, example = "1000")
	private int codSucursal;

	public InfFinanciera() {
		super();
	}

	public int getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(int tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getIdTercero() {
		return idTercero;
	}

	public void setIdTercero(String idTercero) {
		this.idTercero = idTercero;
	}

	public int getSueldos() {
		return sueldos;
	}

	public void setSueldos(int sueldos) {
		this.sueldos = sueldos;
	}

	public int getBonificaciones() {
		return bonificaciones;
	}

	public void setBonificaciones(int bonificaciones) {
		this.bonificaciones = bonificaciones;
	}

	public int getHorasExtra() {
		return horasExtra;
	}

	public void setHorasExtra(int horasExtra) {
		this.horasExtra = horasExtra;
	}

	public int getPrimas() {
		return primas;
	}

	public void setPrimas(int primas) {
		this.primas = primas;
	}

	public int getHonorarios() {
		return honorarios;
	}

	public void setHonorarios(int honorarios) {
		this.honorarios = honorarios;
	}

	public int getOtrosIngresos() {
		return otrosIngresos;
	}

	public void setOtrosIngresos(int otrosIngresos) {
		this.otrosIngresos = otrosIngresos;
	}

	public int getDescuentosLey() {
		return descuentosLey;
	}

	public void setDescuentosLey(int descuentosLey) {
		this.descuentosLey = descuentosLey;
	}

	public int getPrestamosNomina() {
		return prestamosNomina;
	}

	public void setPrestamosNomina(int prestamosNomina) {
		this.prestamosNomina = prestamosNomina;
	}

	public int getObliHipotecas() {
		return obliHipotecas;
	}

	public void setObliHipotecas(int obliHipotecas) {
		this.obliHipotecas = obliHipotecas;
	}

	public int getObliFinancieras() {
		return obliFinancieras;
	}

	public void setObliFinancieras(int obliFinancieras) {
		this.obliFinancieras = obliFinancieras;
	}

	public int getGatosFamilia() {
		return gatosFamilia;
	}

	public void setGatosFamilia(int gatosFamilia) {
		this.gatosFamilia = gatosFamilia;
	}

	public int getOtrosGastos() {
		return otrosGastos;
	}

	public void setOtrosGastos(int otrosGastos) {
		this.otrosGastos = otrosGastos;
	}

	public Date getFecDesprendible() {
		return fecDesprendible;
	}

	public void setFecDesprendible(Date fecDesprendible) {
		this.fecDesprendible = fecDesprendible;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Date getFecEstado() {
		return fecEstado;
	}

	public void setFecEstado(Date fecEstado) {
		this.fecEstado = fecEstado;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getPagaduria() {
		return pagaduria;
	}

	public void setPagaduria(String pagaduria) {
		this.pagaduria = pagaduria;
	}

	public int getCodSucursal() {
		return codSucursal;
	}

	public void setCodSucursal(int codSucursal) {
		this.codSucursal = codSucursal;
	}

}
