package com.bancopopular.servicios.entidadesdeservicio;

import java.util.Date;
import java.util.List;

public class RespuestaConsultarInformacionFinanciera {

	private double sueldo;
	private double egresos;
	private Date fechaDesprendible;
	private String hola;
	private List<String> listadoErrores;

	public RespuestaConsultarInformacionFinanciera() {
		hola = "Cambio 3:11 PM!";
	}

	public String getHola() {
		return hola;
	}

	public void setHola(String hola) {
		this.hola = hola;
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}

	public double getEgresos() {
		return egresos;
	}

	public void setEgresos(double egresos) {
		this.egresos = egresos;
	}

	public Date getFechaDesprendible() {
		return fechaDesprendible;
	}

	public void setFechaDesprendible(Date fechaDesprendible) {
		this.fechaDesprendible = fechaDesprendible;
	}

	public List<String> getListadoErrores() {
		return listadoErrores;
	}

	public void setListadoErrores(List<String> listadoErrores) {
		this.listadoErrores = listadoErrores;
	}

}
