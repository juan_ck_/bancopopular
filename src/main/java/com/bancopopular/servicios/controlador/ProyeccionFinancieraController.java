package com.bancopopular.servicios.controlador;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bancopopular.servicios.entidades.IPC;
import com.bancopopular.servicios.entidadesdeservicio.RespuestaConsultarInformacionFinanciera;
import com.bancopopular.servicios.entidadesentrada.ObjetoPeticion;
import com.bancopopular.servicios.excepciones.BadRequestException;
import com.bancopopular.servicios.logicanegocio.ProyeccionFinanciera;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("${application.deimos.api.path}/libranza")
@Api(tags = "libranza", description = "Consulta de libranza de un cliente")
@Slf4j
public class ProyeccionFinancieraController {

	@Autowired
	private ProyeccionFinanciera proyeccionFinanciera;

	@GetMapping(value = "/consulta_ipc", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Consulta de IPC", response = IPC.class, httpMethod = "GET", notes = "classpath:swagger/notes/proyeccion-libranza.md")
	@ApiResponses({ @ApiResponse(code = 200, message = "Se ejecuto satisfactoriamente."),
			@ApiResponse(code = 400, message = "El cliente envio datos incorrectos."),
			@ApiResponse(code = 500, message = "Error inesperado.") })

	public List<IPC> consultaIPC() {
		List<IPC> listaIPCs = proyeccionFinanciera.consultarIPCs();
		return listaIPCs;
	}

	@GetMapping(value = "/consultarInformacionFinanciera", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Consulta de toda la informacion", response = IPC.class, httpMethod = "GET", notes = "classpath:swagger/notes/proyeccion-libranza.md")
	@ApiResponses({ @ApiResponse(code = 200, message = "Se ejecuto satisfactoriamente."),
			@ApiResponse(code = 400, message = "El cliente envio datos incorrectos."),
			@ApiResponse(code = 500, message = "Error inesperado.") })
	public RespuestaConsultarInformacionFinanciera prueba(@RequestParam int tipoDocumento,
			@RequestParam String numeroDocumento, @RequestParam String nitPagaduria, @RequestParam int sector)
			throws BadRequestException {
		try {
		return proyeccionFinanciera.consultarInformacionFinanciera(tipoDocumento, numeroDocumento, nitPagaduria,
				sector);
		} catch(Exception e) {
			throw new BadRequestException("se da�o esto");
		}
	}
	
	@PostMapping("/post")
	@ResponseBody
	public RespuestaConsultarInformacionFinanciera consultarInformacion(@Valid @RequestBody ObjetoPeticion objeto) throws BadRequestException, NumberFormatException {
		
		return proyeccionFinanciera.consultarInformacion(objeto.getTipoDocumento(), objeto.getNumeroDocumento(), objeto.getNitPagaduria(),
				objeto.getSector());
	}

}
