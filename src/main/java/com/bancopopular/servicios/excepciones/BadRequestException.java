package com.bancopopular.servicios.excepciones;

public class BadRequestException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String DESCRIPTION = "bad request";

	public BadRequestException() {
		super(DESCRIPTION);
	}

	public BadRequestException(String detail) {
		super(DESCRIPTION + ". " + detail);
	}
}
