package com.bancopopular.servicios.excepciones;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ApiExceptionControlador {

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({Exception.class, BadRequestException.class})
	@ResponseBody
	public ErrorMessage badRequest(Exception exception) {
		return new ErrorMessage(exception/*, ""*/);
	}
}
